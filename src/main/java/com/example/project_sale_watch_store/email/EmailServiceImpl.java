package com.example.project_sale_watch_store.email;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService{

    private final JavaMailSender javaMailSender;

    @Override
    public void send(String to, String email, String subject) throws MessagingException, UnsupportedEncodingException {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,true,"utf-8");
        helper.setTo(to);
        helper.setText(email,true);
        helper.setSubject(subject);
        helper.setFrom("TimeTrove@gmail.com", "TimeTrove.com");

        javaMailSender.send(message);
    }

    @Override
    public String buildEmailWelcome(String email) {
        return null;
    }

    @Override
    public String buildEmail(String name, String link, String password, boolean isValidEmail) {
        String send = "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote> Link will expire in 15 minutes. <p>See you soon</p>";
        String title = "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>";
        if (isValidEmail) {
            send = "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Password: " + password + "</p>";
            title = "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your password</span>";
        }
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">" +
                "" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>" +
                "" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">" +
                "    <tbody><tr>" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">" +
                "        " +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">" +
                "          <tbody><tr>" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">" +
                "                  <tbody><tr>" +
                "                    <td style=\"padding-left:10px\">" +
                "                  " +
                "                    </td>" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">" +
                title +
                "                    </td>" +
                "                  </tr>" +
                "                </tbody></table>" +
                "              </a>" +
                "            </td>" +
                "          </tr>" +
                "        </tbody></table>" +
                "        " +
                "      </td>" +
                "    </tr>" +
                "  </tbody></table>" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">" +
                "    <tbody><tr>" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>" +
                "      <td>" +
                "        " +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">" +
                "                  <tbody><tr>" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>" +
                "                  </tr>" +
                "                </tbody></table>" +
                "        " +
                "      </td>" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>" +
                "    </tr>" +
                "  </tbody></table>" +
                "" +
                "" +
                "" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">" +
                "    <tbody><tr>" +
                "      <td height=\"30\"><br></td>" +
                "    </tr>" +
                "    <tr>" +
                "      <td width=\"10\" valign=\"middle\"><br></td>" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">" +
                "        " +
                send +
                "        " +
                "      </td>" +
                "      <td width=\"10\" valign=\"middle\"><br></td>" +
                "    </tr>" +
                "    <tr>" +
                "      <td height=\"30\"><br></td>" +
                "    </tr>" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">" +
                "" +
                "</div></div>";
    }

    @Override
    public String buildEmailCongratulation(String email) {
        return null;
    }

    @Override
    public String buildEmailVerify(String email) {
        return "<div style=\"font-family: 'Arial', sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; text-align: center; color: #333333;\">\n" +
                "    <div style=\"background-color: #ffffff; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); padding: 30px; margin: 20px auto; max-width: 600px;\">\n" +
                "        <h1 style=\"color: #4caf50;\">Staying.com</h1>\n" +
                "        \n" +
                "        <p style=\"margin-bottom: 15px;\">Xin chào [Tên đối tác],</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi rất vui mừng thông báo rằng quá trình xác nhận các điều khoản đã hoàn tất thành công. Đây là bước quan trọng để chúng ta có thể tiếp tục hợp tác một cách hiệu quả.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi đã cập nhật thông tin về các điều khoản trong hợp đồng và bạn có thể xem chi tiết bằng cách truy cập vào <a href=\"[Liên kết đến trang chi tiết điều khoản]\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">trang chi tiết điều khoản</a>.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Nếu bạn có bất kỳ thắc mắc hoặc cần thêm thông tin, đừng ngần ngại liên hệ với chúng tôi qua địa chỉ email: <a href=\"mailto:lienhe@example.com\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">lienhe@example.com</a>.</p>\n" +
                "\n" +
                "        <!-- Thay đổi tên đối tác và địa chỉ email bên dưới bằng thông tin thực tế của đối tác -->\n" +
                "        <p style=\"text-align: center; margin-bottom: 0; color: #333333;\">Trân trọng,<br>[Tên Công Ty]</p>\n" +
                "    </div>\n" +
                "</div>";
    }

    @Override
    public String buildEmailContract(String mail) {
        return null;
    }

    @Override
    public String buildEmailWaitForConfirmation(String mail) {
        return null;
    }
}
