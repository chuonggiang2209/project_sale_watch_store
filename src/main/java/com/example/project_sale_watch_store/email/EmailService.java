package com.example.project_sale_watch_store.email;

import jakarta.mail.MessagingException;

import java.io.UnsupportedEncodingException;

public interface EmailService {

    void send(String to, String email, String subject) throws MessagingException, UnsupportedEncodingException;

    String buildEmailWelcome(String email);

    String buildEmail(String name, String link, String password, boolean isValidEmail);

    String buildEmailCongratulation(String email);

    String buildEmailVerify(String email);

    String buildEmailContract(String mail);

    String buildEmailWaitForConfirmation(String mail);

}
