package com.example.project_sale_watch_store.orderdetail.mapper;

import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.orderdetail.entity.OrderDetail;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class OrderDetailDTOMapper implements Function<OrderDetail, OrderDetailDTO> {

    @Override
    public OrderDetailDTO apply(OrderDetail orderDetail) {
        return OrderDetailDTO.builder()
                .productId(orderDetail.getProduct().getId())
                .price(orderDetail.getPrice())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                .build();
    }
}
