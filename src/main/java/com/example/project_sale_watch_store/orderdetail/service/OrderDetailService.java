package com.example.project_sale_watch_store.orderdetail.service;

import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.orderdetail.dto.request.OrderDetailAddRequest;

public interface OrderDetailService {

    OrderDetailDTO addProductDetail(OrderDetailAddRequest orderDetailAddRequest, Order order);
}
