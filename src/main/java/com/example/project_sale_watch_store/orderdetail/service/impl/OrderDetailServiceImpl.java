package com.example.project_sale_watch_store.orderdetail.service.impl;

import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.orderdetail.dto.request.OrderDetailAddRequest;
import com.example.project_sale_watch_store.orderdetail.entity.OrderDetail;
import com.example.project_sale_watch_store.orderdetail.mapper.OrderDetailDTOMapper;
import com.example.project_sale_watch_store.orderdetail.repo.OrderDetailRepo;
import com.example.project_sale_watch_store.orderdetail.service.OrderDetailService;
import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderDetailServiceImpl implements OrderDetailService {

    private final OrderDetailRepo orderDetailRepo;

    private final ProductService productService;

    private final OrderDetailDTOMapper orderDetailDTOMapper;

    @Override
    public OrderDetailDTO addProductDetail(OrderDetailAddRequest orderDetailAddRequest, Order order) {

        Product product = productService.getProductById(orderDetailAddRequest.getProductId());

        OrderDetail orderDetail = OrderDetail.builder()
                .order(order)
                .product(product)
                .quantity(orderDetailAddRequest.getQuantity())
                .price(orderDetailAddRequest.getPrice())
                .totalPrice(orderDetailAddRequest.getTotalPrice())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.ACTIVE_STATUS)
                .build();
        orderDetailRepo.save(orderDetail);

        return orderDetailDTOMapper.apply(orderDetail);
    }
}
