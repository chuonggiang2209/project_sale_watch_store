package com.example.project_sale_watch_store.orderdetail.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class OrderDetailDTO {

    private UUID productId;

    private Integer quantity;

    private Double price;

    private Double totalPrice;
}
