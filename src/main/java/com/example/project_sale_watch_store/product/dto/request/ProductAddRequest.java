package com.example.project_sale_watch_store.product.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class ProductAddRequest {

        private String name;

        private String description;

        private Double price;

        private Integer quantity;

        private Boolean isDeleted;

        private String status;

        private UUID brandId;

        private UUID categoryId;
}
