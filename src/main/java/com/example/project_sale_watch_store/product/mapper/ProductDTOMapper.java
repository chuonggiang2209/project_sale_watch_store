package com.example.project_sale_watch_store.product.mapper;

import com.example.project_sale_watch_store.product.dto.ProductDTO;
import com.example.project_sale_watch_store.product.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class ProductDTOMapper implements Function<Product, ProductDTO> {

    @Override
    public ProductDTO apply(Product product) {
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .isDeleted(product.getIsDeleted())
                .status(product.getStatus())
                .brandId(product.getBrand().getId())
                .categoryId(product.getCategory().getId())
                .build();
    }
}
