package com.example.project_sale_watch_store.product.repo;

import com.example.project_sale_watch_store.product.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepo extends JpaRepository<Product, UUID> {

    Boolean existsByName(String name);

    Optional<Page<Product>> findAllByIsDeletedAndStatus(Boolean isDeleted, String status, Pageable pageable);
}
