package com.example.project_sale_watch_store.product.service.impl;

import com.example.project_sale_watch_store.brand.entity.Brand;
import com.example.project_sale_watch_store.brand.service.BrandService;
import com.example.project_sale_watch_store.category.entity.Category;
import com.example.project_sale_watch_store.category.service.CategoryService;
import com.example.project_sale_watch_store.constant.ProductResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.product.dto.ProductDTO;
import com.example.project_sale_watch_store.product.dto.request.ProductAddRequest;
import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.product.mapper.ProductDTOMapper;
import com.example.project_sale_watch_store.product.repo.ProductRepo;
import com.example.project_sale_watch_store.product.service.ProductService;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final BrandService brandService;

    private final CategoryService categoryService;

    private final ProductRepo productRepo;

    private final ProductDTOMapper productDTOMapper;

    @Override
    public ResponseEntity<Response> addProduct(ProductAddRequest productAddRequest) {

        if (checkProductName(productAddRequest.getName().toLowerCase()))
            throw new ApiRequestException(ResourceConstant.CM_06, ProductResourceConstant.PR_CM_06);

        Brand brand = brandService.getBrandById(productAddRequest.getBrandId());
        Category category = categoryService.getCategoryById(productAddRequest.getCategoryId());

        Product save = productRepo.save(Product.builder()
                .name(productAddRequest.getName().toLowerCase())
                .description(productAddRequest.getDescription())
                .price(productAddRequest.getPrice())
                .quantity(productAddRequest.getQuantity())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.NO_ACTIVE_STATUS)
                .brand(brand)
                .category(category)
                .build());

        Response response = Response.builder()
                .code(ResourceConstant.CM_01)
                .message(ProductResourceConstant.PR_CM_01)
                .data(productDTOMapper.apply(save))
                .status(HttpStatus.CREATED.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateProduct(ProductAddRequest productAddRequest, UUID id) {

        Product product = getProductById(id);
        Brand brand = brandService.getBrandById(productAddRequest.getBrandId());
        Category category = categoryService.getCategoryById(productAddRequest.getCategoryId());
        product.setBrand(brand);
        product.setCategory(category);
        product.setName(productAddRequest.getName().toLowerCase());
        product.setDescription(productAddRequest.getDescription());
        product.setPrice(productAddRequest.getPrice());
        product.setQuantity(productAddRequest.getQuantity());
        product.setStatus(productAddRequest.getStatus());
        product.setIsDeleted(productAddRequest.getIsDeleted());
        productRepo.save(product);

        Response response = Response.builder()
                .code(ResourceConstant.CM_02)
                .message(ProductResourceConstant.PR_CM_02)
                .data(productDTOMapper.apply(product))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteProduct(UUID id) {

        Product product = getProductById(id);
        product.setIsDeleted(SystemConstant.IS_DELETED);
        product.setStatus(SystemConstant.NO_ACTIVE_STATUS);

        productRepo.save(product);

        Response response = Response.builder()
                .code(ResourceConstant.CM_03)
                .message(ProductResourceConstant.PR_CM_03)
                .data(productDTOMapper.apply(product))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Responses> getProducts(Integer currentPage, Integer limitPage) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Product> all = productRepo.findAllByIsDeletedAndStatus(SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_05, ProductResourceConstant.PR_CM_05));

        List<ProductDTO> list = all.stream()
                .map(productDTOMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_07)
                .status(HttpStatus.OK.value())
                .message(ProductResourceConstant.PR_CM_07)
                .data(list)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public Boolean checkProductName(String name) {

        return productRepo.existsByName(name);
    }

    @Override
    public Product getProductById(UUID id) {

        return productRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_04, ProductResourceConstant.PR_CM_04));
    }
}
