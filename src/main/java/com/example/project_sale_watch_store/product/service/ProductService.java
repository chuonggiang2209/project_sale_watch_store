package com.example.project_sale_watch_store.product.service;

import com.example.project_sale_watch_store.product.dto.request.ProductAddRequest;
import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface ProductService {

    ResponseEntity<Response> addProduct(ProductAddRequest productAddRequest);

    ResponseEntity<Response> updateProduct(ProductAddRequest productAddRequest, UUID id);

    ResponseEntity<Response> deleteProduct(UUID id);

    ResponseEntity<Responses> getProducts(Integer currentPage, Integer limitPage);

    Boolean checkProductName(String name);

    Product getProductById(UUID id);
}
