package com.example.project_sale_watch_store.product.controller.pub;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_PUBLIC + APIConstant.API_VERSION_ONE + APIConstant.API_PRODUCT)
public class ProductPublicController {

    private final ProductService productService;

    @GetMapping
    public ResponseEntity<?> getProducts(@RequestParam Optional<Integer> currentPage, @RequestParam Optional<Integer> limitPage) {
        return productService.getProducts(currentPage.orElse(1), limitPage.orElse(8));
    }
}
