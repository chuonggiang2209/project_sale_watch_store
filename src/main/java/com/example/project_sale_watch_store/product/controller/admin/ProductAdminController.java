package com.example.project_sale_watch_store.product.controller.admin;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.product.dto.request.ProductAddRequest;
import com.example.project_sale_watch_store.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_PRODUCT)
public class ProductAdminController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<?> addProduct(@RequestBody ProductAddRequest productAddRequest) {
        return productService.addProduct(productAddRequest);
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<?> updateProduct(@RequestBody ProductAddRequest productAddRequest, @PathVariable UUID id) {
        return productService.updateProduct(productAddRequest, id);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<?> deleteProduct(@PathVariable UUID id) {
        return productService.deleteProduct(id);
    }
}
