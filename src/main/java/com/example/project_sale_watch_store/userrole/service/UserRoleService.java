package com.example.project_sale_watch_store.userrole.service;

import com.example.project_sale_watch_store.user.entity.User;

public interface UserRoleService {

    void addRoleUser(User user);

    void addRoleAdmin(User user);
}
