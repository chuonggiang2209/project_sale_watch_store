package com.example.project_sale_watch_store.userrole.service.impl;

import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.RoleResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.constant.UserResourceConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.role.entity.Role;
import com.example.project_sale_watch_store.role.service.RoleService;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.userrole.entity.UserRole;
import com.example.project_sale_watch_store.userrole.repo.UserRoleRepo;
import com.example.project_sale_watch_store.userrole.service.UserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepo userRoleRepo;

    private final RoleService roleService;

    @Override
    public void addRoleUser(User user) {

        if (user == null)
            throw new ApiRequestException(ResourceConstant.CM_05, UserResourceConstant.US_CM_05);

        Role role = roleService.getRole(RoleResourceConstant.ROLE_USER)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, RoleResourceConstant.RO_CM_06));

        userRoleRepo.save(UserRole.builder()
                .role(role)
                .user(user)
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.ACTIVE_STATUS)
                .build());
    }

    @Override
    public void addRoleAdmin(User user) {

        if (user == null)
            throw new ApiRequestException(ResourceConstant.CM_05, UserResourceConstant.US_CM_05);

        Role role = roleService.getRole(RoleResourceConstant.ROLE_ADMIN)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, RoleResourceConstant.RO_CM_06));

        userRoleRepo.save(UserRole.builder()
                .role(role)
                .user(user)
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.ACTIVE_STATUS)
                .build());
    }
}
