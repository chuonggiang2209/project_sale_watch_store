package com.example.project_sale_watch_store.user.dto.response;

import java.util.UUID;

public record UserTokenResponse(
        UUID id,

        String email,

        String status,

        Boolean isDeleted,

        String token
) {
}
