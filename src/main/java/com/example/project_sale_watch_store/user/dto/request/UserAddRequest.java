package com.example.project_sale_watch_store.user.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class UserAddRequest {

    private String userName;

    private String password;

    private String fullName;

    private String email;

    private String address;

    private Date dateOfBirth;

    private String phoneNumber;

    private String avatar;

    private Boolean isDeleted;

    private String status;

}
