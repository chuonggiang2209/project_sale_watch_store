package com.example.project_sale_watch_store.user.service.impl;

import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.constant.UserResourceConstant;
import com.example.project_sale_watch_store.email.EmailService;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.jwt.JwtHelper;
import com.example.project_sale_watch_store.user.dto.UserDTO;
import com.example.project_sale_watch_store.user.dto.request.UserAddRequest;
import com.example.project_sale_watch_store.user.dto.response.UserAndRoleResponse;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.user.mapper.UserDTOMapper;
import com.example.project_sale_watch_store.user.repo.UserRepo;
import com.example.project_sale_watch_store.user.service.UserService;
import com.example.project_sale_watch_store.userrole.service.UserRoleService;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;

    private final EmailService emailService;

    private final PasswordEncoder passwordEncoder;

    private final UserDTOMapper userDtoMapper;

    private final UserRoleService userRole;

    private final AuthenticationManager authenticationManager;

    private final JwtHelper jwtHelper;

    @Override
    public ResponseEntity<Response> Register(UserAddRequest userAddRequest) throws MessagingException, UnsupportedEncodingException {

        if (userRepo.existsUserByEmail(userAddRequest.getEmail()))
            throw new ApiRequestException(ResourceConstant.CM_04, UserResourceConstant.US_CM_04);

        User user = userRepo.save(
                User.builder()
                        .email(userAddRequest.getEmail())
                        .password(passwordEncoder.encode(userAddRequest.getPassword()))
                        .dateOfBirth(new SimpleDateFormat("dd-MM-yyyy").format(userAddRequest.getDateOfBirth()))
                        .status(SystemConstant.NO_ACTIVE_STATUS)
                        .isDeleted(SystemConstant.IS_NOT_DELETED)
                        .fullName(userAddRequest.getFullName())
                        .avatar(userAddRequest.getAvatar())
                        .phoneNumber(userAddRequest.getPhoneNumber())
                        .address(userAddRequest.getAddress())
                        .userName(userAddRequest.getUserName())
                        .build()
        );

        String buildEmail = emailService.buildEmail(userAddRequest.getEmail(), "http://localhost:8080/public/v1/user/active/" + user.getId(), userAddRequest.getPassword(), false);
        emailService.send(user.getEmail(), buildEmail, "Confirm your email");

        Response response = Response.builder()
                .status(HttpStatus.CREATED.value())
                .code(ResourceConstant.CM_01)
                .data(userDtoMapper.apply(user))
                .message(UserResourceConstant.US_CM_01)
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateUser(UserAddRequest userAddRequest, UUID id) throws ParseException {

        User user = getUserById(id);
        user.setUserName(userAddRequest.getUserName());
        user.setFullName(userAddRequest.getFullName());
        user.setAddress(userAddRequest.getAddress());
        user.setAvatar(userAddRequest.getAvatar());
        user.setDateOfBirth(new SimpleDateFormat("dd-MM-yyyy").format(userAddRequest.getDateOfBirth()));
        user.setPhoneNumber(userAddRequest.getPhoneNumber());

        userRepo.save(user);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_02)
                .data(userDtoMapper.apply(user))
                .message(UserResourceConstant.US_CM_02)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteUser(UUID id) {

        User user = getUserById(id);
        user.setIsDeleted(SystemConstant.IS_DELETED);
        user.setStatus(SystemConstant.NO_ACTIVE_STATUS);
        userRepo.save(user);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_03)
                .data(userDtoMapper.apply(user))
                .message(UserResourceConstant.US_CM_03)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public User getUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = null;

        if (!(authentication == null || !authentication.isAuthenticated())) {
            UserAndRoleResponse userAndRoleResponse = null;

            if (authentication.getPrincipal() instanceof UserAndRoleResponse) {
                userAndRoleResponse = (UserAndRoleResponse) authentication.getPrincipal();
            }

            if (userAndRoleResponse != null) {
                user = getUserByEmail(userAndRoleResponse.getEmail());
            }
        }
        return user;
    }

    @Override
    public User getUserById(UUID id) {

        return userRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_05, UserResourceConstant.US_CM_05));
    }

    @Override
    public ResponseEntity<Responses> getUsers(Integer currentPage, Integer limitPage) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<User> all = userRepo.findAllByIsDeletedAndStatus(SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_08, UserResourceConstant.US_CM_08));
        List<UserDTO> users = all.stream()
                .map(userDtoMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_09)
                .status(HttpStatus.OK.value())
                .message(UserResourceConstant.US_CM_09)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .data(users)
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public User getUserByEmail(String email) {

        return userRepo.findUserByEmail(email.toLowerCase())
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_05, UserResourceConstant.US_CM_05));
    }

    @Override
    public ResponseEntity<Response> activeUser(UUID id) {

        User user = getUserById(id);
        user.setStatus(SystemConstant.ACTIVE_STATUS);
        user.setIsDeleted(SystemConstant.IS_NOT_DELETED);

        userRepo.save(user);

        if (user.getEmail().equalsIgnoreCase("cgianghai@gmail.com")) {
            userRole.addRoleAdmin(user);
        } else {
            userRole.addRoleUser(user);
        }

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_02)
                .data(userDtoMapper.apply(user))
                .message(UserResourceConstant.US_CM_02)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> changePassword(UserAddRequest userAddRequest, UUID id) {

        User user = getUserById(id);

        if (user == null)
            throw new ApiRequestException(ResourceConstant.CM_05, UserResourceConstant.US_CM_05);

        User userchangePassword = getUserById(user.getId());
        userchangePassword.setPassword(passwordEncoder.encode(userAddRequest.getPassword()));
        userRepo.save(userchangePassword);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_02)
                .data(userDtoMapper.apply(userchangePassword))
                .message(UserResourceConstant.US_CM_02)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> authenticate(UserAddRequest userAddRequest) {

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userAddRequest.getEmail(), userAddRequest.getPassword());
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        User user = (User) authenticate.getPrincipal();

        List<String> roles = authenticate.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();

        UserAndRoleResponse userAndRoleResponse = UserAndRoleResponse.builder()
                .id(user.getId())
                .email(user.getEmail())
                .roles(roles)
                .build();
        String token = jwtHelper.generateToken(userAndRoleResponse);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_06)
                .data(userDtoMapper.applyToken(user, token))
                .message(UserResourceConstant.US_CM_06)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> logout() {
        return null;
    }

    @Override
    public ResponseEntity<Response> forgotPassword(UserAddRequest userAddRequest) throws MessagingException, UnsupportedEncodingException {

        User user = getUserByEmail(userAddRequest.getEmail());

        int length = 10;
        String ranStr = IntStream.range(0, length)
                .mapToObj(i -> String.valueOf((char) ('a' + ThreadLocalRandom.current().nextInt(26))))
                .collect(Collectors.joining());
        String email = ranStr + ranStr.substring(3, 5).toUpperCase();
        emailService.send(user.getEmail(), email, "Your New Password");
        user.setPassword(passwordEncoder.encode(email));

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_02)
                .data(userDtoMapper.apply(user))
                .message(UserResourceConstant.US_CM_02)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
