package com.example.project_sale_watch_store.user.service;

import com.example.project_sale_watch_store.user.dto.UserDTO;
import com.example.project_sale_watch_store.user.dto.request.UserAddRequest;
import com.example.project_sale_watch_store.user.dto.response.UserAndRoleResponse;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import jakarta.mail.MessagingException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.UUID;

public interface UserService {

    ResponseEntity<Response> Register(UserAddRequest userAddRequest) throws ParseException, MessagingException, UnsupportedEncodingException;

    ResponseEntity<Response> updateUser(UserAddRequest userAddRequest, UUID uuid) throws ParseException;

    ResponseEntity<Response> deleteUser(UUID id);

    User getUser();

    User getUserById(UUID id);

    ResponseEntity<Responses> getUsers(Integer currentPage, Integer limitPage);

    User getUserByEmail(String email);

    ResponseEntity<Response> activeUser(UUID id);

    ResponseEntity<Response> changePassword(UserAddRequest userAddRequest, UUID id);

    ResponseEntity<Response> authenticate(UserAddRequest userAddRequest);

    ResponseEntity<Response> logout();

    ResponseEntity<Response> forgotPassword(UserAddRequest userAddRequest) throws MessagingException, UnsupportedEncodingException;
}
