package com.example.project_sale_watch_store.user.mapper;

import com.example.project_sale_watch_store.user.dto.UserDTO;
import com.example.project_sale_watch_store.user.dto.response.UserTokenResponse;
import com.example.project_sale_watch_store.user.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class UserDTOMapper implements Function<User, UserDTO> {

    @Override
    public UserDTO apply(User user) {

        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .status(user.getStatus())
                .isDeleted(user.getIsDeleted())
                .build();
    }

    public UserTokenResponse applyToken(User user, String token) {

        return new UserTokenResponse(user.getId(), user.getEmail(), user.getStatus(), user.getIsDeleted(), token);
    }


}
