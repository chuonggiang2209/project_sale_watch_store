package com.example.project_sale_watch_store.user.entity;


import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.userrole.entity.UserRole;
import com.example.project_sale_watch_store.utils.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_user")
public class User extends BaseEntity {

    @Column(name = "user_name", nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9]{5,}$",
            message = "Username must contain at least 5 characters")
    private String userName;

    @Column(name = "password", nullable = false)
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$",
            message = "Password must contain at least 8 characters, including UPPER/lowercase and numbers")
    private String password;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Email(message = "Email should be valid")
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "dayOfBirth", nullable = false)
    private String dateOfBirth;

    @Column(name = "phone_number", nullable = false)
    @Pattern(regexp = "^[0-9]{10,11}$",
            message = "Phone number must contain at least 10 characters")
    private String phoneNumber;

    private String avatar;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Order> orders;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<UserRole> userRoles;

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", avatar='" + avatar + '\'' +
                ", orders=" + orders +
                ", userRoles=" + userRoles +
                '}';
    }
}
