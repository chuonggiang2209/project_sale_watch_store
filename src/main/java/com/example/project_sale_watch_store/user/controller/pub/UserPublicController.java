package com.example.project_sale_watch_store.user.controller.pub;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.user.dto.request.UserAddRequest;
import com.example.project_sale_watch_store.user.service.UserService;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_PUBLIC + APIConstant.API_VERSION_ONE + APIConstant.API_USER)
public class UserPublicController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<?> Register(@Validated @RequestBody UserAddRequest userAddRequest) throws ParseException, MessagingException, UnsupportedEncodingException {
        return userService.Register(userAddRequest);
    }

    @GetMapping("/active/{id}")
    public ResponseEntity<?> activeUser(@PathVariable UUID id) {
        return userService.activeUser(id);
    }


    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserAddRequest userAddRequest) {
        return userService.authenticate(userAddRequest);
    }

    @PutMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestBody UserAddRequest userAddRequest) throws MessagingException, UnsupportedEncodingException {
        return userService.forgotPassword(userAddRequest);
    }
}
