package com.example.project_sale_watch_store.user.controller.member;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.user.dto.request.UserAddRequest;
import com.example.project_sale_watch_store.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_MEMBER + APIConstant.API_VERSION_ONE + APIConstant.API_USER)
public class UserMemberController {

    private final UserService userService;

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserAddRequest userAddRequest, @PathVariable UUID id) throws ParseException {
        return userService.updateUser(userAddRequest, id);
    }

    @PutMapping("/change/{id}")
    public ResponseEntity<?> changePassword(@RequestBody UserAddRequest userAddRequest, @PathVariable UUID id) {
        return userService.changePassword(userAddRequest,id);
    }
}
