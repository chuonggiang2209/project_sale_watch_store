package com.example.project_sale_watch_store.user.repo;

import com.example.project_sale_watch_store.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepo extends JpaRepository<User, UUID> {

    Boolean existsUserByEmail(String email);

    Optional<User> findUserByEmail(String email);

    Optional<Page<User>> findAllByIsDeletedAndStatus(Boolean isDeleted, String status, Pageable pageable);


}
