package com.example.project_sale_watch_store.configuration;

import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.RoleResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.constant.UserResourceConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.user.repo.UserRepo;
import com.example.project_sale_watch_store.userrole.entity.UserRole;
import com.example.project_sale_watch_store.userrole.repo.UserRoleRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomAuthenticateProvider implements AuthenticationProvider {

    private final UserRepo userRepo;

    private final PasswordEncoder passwordEncoder;

    private final UserRoleRepo userRoleRepo;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String email = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        User user = userRepo.findUserByEmail(email)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_07, UserResourceConstant.US_CM_07));
        SecurityContext securityContext = SecurityContextHolder.getContext();

        if (user != null && passwordEncoder.matches(password, user.getPassword())) {

            List<UserRole> userRoles = userRoleRepo.findAllByUserId(user.getId())
                    .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_08, RoleResourceConstant.RO_CM_08));
            List<String> roles = userRoles.stream()
                    .map(userRole -> userRole.getRole().getName())
                    .toList();
            List<GrantedAuthority> grantedAuthorities = roles.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());

            if (user.getStatus().equals(SystemConstant.NO_ACTIVE_STATUS))
                throw new ApiRequestException(ResourceConstant.CM_10, UserResourceConstant.US_CM_10);

            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
            securityContext.setAuthentication(authenticationToken);
        } else {
            throw new ApiRequestException(ResourceConstant.CM_07, UserResourceConstant.US_CM_07);
        }

        return securityContext.getAuthentication();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
