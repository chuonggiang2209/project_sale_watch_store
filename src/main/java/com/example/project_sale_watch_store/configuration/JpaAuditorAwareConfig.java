package com.example.project_sale_watch_store.configuration;

import com.example.project_sale_watch_store.user.dto.UserDTO;
import com.example.project_sale_watch_store.user.dto.response.UserAndRoleResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class JpaAuditorAwareConfig {

    @Bean
    public AuditorAware<String> auditorAware() {
        return new AuditorAwareImpl();
    }

    public static class AuditorAwareImpl implements AuditorAware<String> {
        @Override
        @NonNull
        public Optional<String> getCurrentAuditor() {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication == null || !authentication.isAuthenticated()) {
                return Optional.empty();
            }

            UserAndRoleResponse UserAndRoleResponse = null;

            if (authentication.getPrincipal() instanceof UserAndRoleResponse) {
                UserAndRoleResponse = (UserAndRoleResponse) authentication.getPrincipal();
            }

            if (UserAndRoleResponse == null)
                return Optional.ofNullable(authentication.getName());

            return Optional.ofNullable(UserAndRoleResponse.getEmail());
        }
    }
}
