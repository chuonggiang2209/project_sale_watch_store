package com.example.project_sale_watch_store.brand.service;

import com.example.project_sale_watch_store.brand.dto.request.BrandAddRequest;
import com.example.project_sale_watch_store.brand.entity.Brand;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface BrandService {

    ResponseEntity<Response> addBrand(BrandAddRequest brandAddRequest);

    ResponseEntity<Response> updateBrand(BrandAddRequest brandAddRequest, UUID brandId);

    ResponseEntity<Response> deleteBrand(UUID brandId);

    ResponseEntity<Responses> getBrands(Integer currentPage, Integer limitPage);

    Brand getBrandById(UUID brandId);

    Boolean checkBrandName(String brandName);
}
