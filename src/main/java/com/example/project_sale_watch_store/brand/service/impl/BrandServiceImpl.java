package com.example.project_sale_watch_store.brand.service.impl;

import com.example.project_sale_watch_store.brand.dto.BrandDTO;
import com.example.project_sale_watch_store.brand.dto.request.BrandAddRequest;
import com.example.project_sale_watch_store.brand.entity.Brand;
import com.example.project_sale_watch_store.brand.mapper.BrandDTOMapper;
import com.example.project_sale_watch_store.brand.repo.BrandRepo;
import com.example.project_sale_watch_store.brand.service.BrandService;
import com.example.project_sale_watch_store.constant.BrandResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {

    private final BrandDTOMapper brandDTOMapper;

    private final BrandRepo brandRepo;

    @Override
    public ResponseEntity<Response> addBrand(BrandAddRequest brandAddRequest) {

        if (checkBrandName(brandAddRequest.getName().toLowerCase()))
            throw new ApiRequestException(ResourceConstant.CM_06, BrandResourceConstant.BR_CM_06);

        Brand brand = Brand.builder()
                .name(brandAddRequest.getName().toLowerCase())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.NO_ACTIVE_STATUS)
                .build();
        brandRepo.save(brand);

        Response response = Response.builder()
                .code(ResourceConstant.CM_01)
                .message(BrandResourceConstant.BR_CM_01)
                .status(HttpStatus.CREATED.value())
                .data(brandDTOMapper.apply(brand))
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateBrand(BrandAddRequest brandAddRequest, UUID brandId) {

        Brand brand = getBrandById(brandId);
        brand.setStatus(brandAddRequest.getStatus());
        brand.setIsDeleted(brandAddRequest.getIsDeleted());
        brandRepo.save(brand);

        Response response = Response.builder()
                .code(ResourceConstant.CM_02)
                .message(BrandResourceConstant.BR_CM_02)
                .status(HttpStatus.OK.value())
                .data(brandDTOMapper.apply(brand))
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteBrand(UUID brandId) {

        Brand brand = getBrandById(brandId);
        brand.setStatus(SystemConstant.NO_ACTIVE_STATUS);
        brand.setIsDeleted(SystemConstant.IS_DELETED);
        brandRepo.save(brand);

        Response response = Response.builder()
                .code(ResourceConstant.CM_03)
                .message(BrandResourceConstant.BR_CM_03)
                .status(HttpStatus.OK.value())
                .data(brandDTOMapper.apply(brand))
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Responses> getBrands(Integer currentPage, Integer limitPage) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Brand> all = brandRepo.findAllByIsDeletedAndStatus(SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_07, BrandResourceConstant.BR_CM_05));
        List<BrandDTO> brandDTOS = all.stream()
                .map(brandDTOMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_07)
                .status(HttpStatus.OK.value())
                .data(brandDTOS)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .message(BrandResourceConstant.BR_CM_07)
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public Boolean checkBrandName(String brandName) {
        return brandRepo.existsByName(brandName);
    }

    @Override
    public Brand getBrandById(UUID brandId) {

        return brandRepo.findById(brandId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_04, BrandResourceConstant.BR_CM_04));
    }
}

