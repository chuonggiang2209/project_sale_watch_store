package com.example.project_sale_watch_store.brand.repo;

import com.example.project_sale_watch_store.brand.entity.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BrandRepo extends JpaRepository<Brand, UUID> {

    Boolean existsByName(String brandName);

    Optional<Page<Brand>> findAllByIsDeletedAndStatus(Boolean isDeleted, String status, Pageable pageable);
}
