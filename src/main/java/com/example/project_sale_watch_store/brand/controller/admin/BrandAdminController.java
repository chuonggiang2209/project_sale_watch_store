package com.example.project_sale_watch_store.brand.controller.admin;

import com.example.project_sale_watch_store.brand.dto.request.BrandAddRequest;
import com.example.project_sale_watch_store.brand.service.BrandService;
import com.example.project_sale_watch_store.constant.APIConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_BRAND)
public class BrandAdminController {

    private final BrandService brandService;

    @PostMapping
    public ResponseEntity<?> addBrand(@RequestBody BrandAddRequest brandAddRequest) {
        return brandService.addBrand(brandAddRequest);
    }

    @GetMapping
    public ResponseEntity<?> getBrands(@RequestParam Optional<Integer> currentPage, @RequestParam Optional<Integer> limitPage) {
        return brandService.getBrands(currentPage.orElse(1), limitPage.orElse(8));
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<?> updateBrand(@RequestBody BrandAddRequest brandAddRequest, @PathVariable UUID id) {
        return brandService.updateBrand(brandAddRequest, id);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<?> deleteBrand(@PathVariable UUID id) {
        return brandService.deleteBrand(id);
    }
}
