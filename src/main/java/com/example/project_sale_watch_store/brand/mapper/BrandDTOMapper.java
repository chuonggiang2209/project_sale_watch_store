package com.example.project_sale_watch_store.brand.mapper;

import com.example.project_sale_watch_store.brand.dto.BrandDTO;
import com.example.project_sale_watch_store.brand.entity.Brand;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BrandDTOMapper implements Function<Brand, BrandDTO> {

    @Override
    public BrandDTO apply(Brand brand) {
        return BrandDTO.builder()
                .id(brand.getId())
                .name(brand.getName())
                .isDeleted(brand.getIsDeleted())
                .status(brand.getStatus())
                .build();
    }
}
