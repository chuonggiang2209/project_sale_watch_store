package com.example.project_sale_watch_store.productdetail.mapper;

import com.example.project_sale_watch_store.productdetail.dto.ProductDetailDTO;
import com.example.project_sale_watch_store.productdetail.entity.ProductDetail;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ProductDetailDTOMapper implements Function<ProductDetail, ProductDetailDTO> {

    @Override
    public ProductDetailDTO apply(ProductDetail productDetail) {
        return ProductDetailDTO.builder()
                .id(productDetail.getId())
                .productId(productDetail.getProduct().getId())
                .SKU(productDetail.getSKU())
                .userTarget(productDetail.getUserTarget())
                .movement(productDetail.getMovement())
                .glassMaterial(productDetail.getGlassMaterial())
                .waterResistance(productDetail.getWaterResistance())
                .origin(productDetail.getOrigin())
                .shape(productDetail.getShape())
                .caseColor(productDetail.getCaseColor())
                .condition(productDetail.getCondition())
                .dialColor(productDetail.getDialColor())
                .build();
    }
}
