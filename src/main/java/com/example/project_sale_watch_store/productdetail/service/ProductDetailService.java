package com.example.project_sale_watch_store.productdetail.service;

import com.example.project_sale_watch_store.productdetail.dto.request.ProductDetailAddRequest;
import com.example.project_sale_watch_store.productdetail.entity.ProductDetail;
import com.example.project_sale_watch_store.utils.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface ProductDetailService {

    ResponseEntity<Response> addProductDetail(ProductDetailAddRequest productDetailAddRequest, UUID id);

    ResponseEntity<Response> updateProductDetail(ProductDetailAddRequest productDetailAddRequest, UUID id);

    ResponseEntity<Response> deleteProductDetail(UUID id);

    ResponseEntity<Response> getProductDetailByProductId(UUID id);
}
