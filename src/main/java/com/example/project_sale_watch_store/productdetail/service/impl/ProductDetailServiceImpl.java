package com.example.project_sale_watch_store.productdetail.service.impl;

import com.example.project_sale_watch_store.constant.ProductDetailResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.product.service.ProductService;
import com.example.project_sale_watch_store.productdetail.dto.request.ProductDetailAddRequest;
import com.example.project_sale_watch_store.productdetail.entity.ProductDetail;
import com.example.project_sale_watch_store.productdetail.mapper.ProductDetailDTOMapper;
import com.example.project_sale_watch_store.productdetail.repo.ProductDetailRepo;
import com.example.project_sale_watch_store.productdetail.service.ProductDetailService;
import com.example.project_sale_watch_store.utils.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class ProductDetailServiceImpl implements ProductDetailService {

    private final ProductDetailRepo productDetailRepo;

    private final ProductService productService;

    private final ProductDetailDTOMapper productDetailDTOMapper;

    @Override
    public ResponseEntity<Response> addProductDetail(ProductDetailAddRequest productDetailAddRequest, UUID id) {

        Product product = productService.getProductById(id);

        ProductDetail productDetail = productDetailRepo.save(ProductDetail.builder()
                .SKU(productDetailAddRequest.getSKU())
                .userTarget(productDetailAddRequest.getUserTarget())
                .movement(productDetailAddRequest.getMovement())
                .glassMaterial(productDetailAddRequest.getGlassMaterial())
                .waterResistance(productDetailAddRequest.getWaterResistance())
                .origin(productDetailAddRequest.getOrigin())
                .shape(productDetailAddRequest.getShape())
                .caseColor(productDetailAddRequest.getCaseColor())
                .condition(productDetailAddRequest.getCondition())
                .dialColor(productDetailAddRequest.getDialColor())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.ACTIVE_STATUS)
                .product(product)
                .build());

        Response response = Response.builder()
                .code(ResourceConstant.CM_01)
                .message(ProductDetailResourceConstant.PRD_CM_01)
                .data(productDetailDTOMapper.apply(productDetail))
                .status(HttpStatus.CREATED.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateProductDetail(ProductDetailAddRequest productDetailAddRequest, UUID id) {

        ProductDetail productDetail = getProductDetailByPdId(id);

        productDetail.setSKU(productDetailAddRequest.getSKU());
        productDetail.setUserTarget(productDetailAddRequest.getUserTarget());
        productDetail.setMovement(productDetailAddRequest.getMovement());
        productDetail.setGlassMaterial(productDetailAddRequest.getGlassMaterial());
        productDetail.setWaterResistance(productDetailAddRequest.getWaterResistance());
        productDetail.setOrigin(productDetailAddRequest.getOrigin());
        productDetail.setShape(productDetailAddRequest.getShape());
        productDetail.setCaseColor(productDetailAddRequest.getCaseColor());
        productDetail.setCondition(productDetailAddRequest.getCondition());
        productDetail.setDialColor(productDetailAddRequest.getDialColor());

        productDetailRepo.save(productDetail);

        Response response = Response.builder()
                .code(ResourceConstant.CM_02)
                .message(ProductDetailResourceConstant.PRD_CM_02)
                .data(productDetailDTOMapper.apply(productDetail))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteProductDetail(UUID id) {
        return null;
    }

    @Override
    public ResponseEntity<Response> getProductDetailByProductId(UUID id) {

        ProductDetail productDetail = getProductDetailByPdId(id);

        Response response = Response.builder()
                .code(ResourceConstant.CM_05)
                .message(ProductDetailResourceConstant.PRD_CM_04)
                .data(productDetailDTOMapper.apply(productDetail))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public ProductDetail getProductDetailByPdId(UUID id) {

        return productDetailRepo.getProductDetailByProductId(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, ProductDetailResourceConstant.PRD_CM_06));
    }
}
