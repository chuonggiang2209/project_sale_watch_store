package com.example.project_sale_watch_store.productdetail.entity;



import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.utils.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
@Table(name = "_product_detail")
public class ProductDetail extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false)
    private Product product;

    @Column(name = "SKU", nullable = false)
    private String SKU;

    @Column(name = "user_target", nullable = false)
    private Boolean userTarget;

    @Column(name = "movement", nullable = false)
    private String movement;

    @Column(name = "glass_material", nullable = false)
    private String glassMaterial;

    @Column(name = "water_Resistance", nullable = false)
    private Boolean waterResistance;

    @Column(name = "origin", nullable = false)
    private String origin;

    @Column(name = "shape", nullable = false)
    private String shape;

    @Column(name = "case_color", nullable = false)
    private String caseColor;

    @Column(name = "condition", nullable = false)
    private Boolean condition;

    @Column(name = "dial_color", nullable = false)
    private String dialColor;
}
