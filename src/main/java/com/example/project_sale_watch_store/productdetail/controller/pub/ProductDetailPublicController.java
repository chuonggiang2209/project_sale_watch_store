package com.example.project_sale_watch_store.productdetail.controller.pub;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.productdetail.service.ProductDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_PUBLIC + APIConstant.API_VERSION_ONE + APIConstant.API_PRODUCT_DETAIL)
public class ProductDetailPublicController {

    private final ProductDetailService productDetailService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getProductDetailByProductId(@PathVariable UUID id) {
        return productDetailService.getProductDetailByProductId(id);
    }
}
