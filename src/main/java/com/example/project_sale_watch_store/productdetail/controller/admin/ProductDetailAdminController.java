package com.example.project_sale_watch_store.productdetail.controller.admin;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.productdetail.dto.request.ProductDetailAddRequest;
import com.example.project_sale_watch_store.productdetail.service.ProductDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_PRODUCT_DETAIL)
public class ProductDetailAdminController {

    private final ProductDetailService productDetailService;

    @PostMapping("/{id}")
    public ResponseEntity<?> addProductDetail(@RequestBody ProductDetailAddRequest productDetailAddRequest, @PathVariable UUID id) {
        return productDetailService.addProductDetail(productDetailAddRequest, id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateProductDetail(@RequestBody ProductDetailAddRequest productDetailAddRequest, @PathVariable UUID id) {
        return productDetailService.updateProductDetail(productDetailAddRequest, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProductDetail(@PathVariable UUID id) {
        return productDetailService.deleteProductDetail(id);
    }
}
