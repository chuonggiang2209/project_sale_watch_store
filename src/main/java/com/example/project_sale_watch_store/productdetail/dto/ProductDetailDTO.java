package com.example.project_sale_watch_store.productdetail.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class ProductDetailDTO {

    private UUID id;

    private UUID productId;

    private String SKU;

    private Boolean userTarget;

    private String movement;

    private String glassMaterial;

    private Boolean waterResistance;

    private String origin;

    private String shape;

    private String caseColor;

    private Boolean condition;

    private String dialColor;

}
