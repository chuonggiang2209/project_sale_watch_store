package com.example.project_sale_watch_store.productdetail.repo;

import com.example.project_sale_watch_store.productdetail.entity.ProductDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductDetailRepo extends JpaRepository<ProductDetail, UUID> {

    Optional<ProductDetail> getProductDetailByProductId(UUID id);
}
