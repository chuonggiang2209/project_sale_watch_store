package com.example.project_sale_watch_store.constant;

public class RoleResourceConstant {

    public final static String RO_CM_01 = "Create Success";
    public final static String RO_CM_02 = "Update Success";
    public final static String RO_CM_03 = "Delete Success";
    public final static String RO_CM_04 = "Role name is duplicated";
    public final static String RO_CM_05 = "Role name is null or empty";
    public final static String RO_CM_06 = "Role not found";
    public final static String RO_CM_07 = "Role name is already exist";
    public final static String RO_CM_08 = "Role list is empty";
    public final static String RO_CM_09 = "Get roles success";
    public final static String ROLE_USER = "ROLE_USER";
    public final static String ROLE_ADMIN = "ROLE_ADMIN";
}
