package com.example.project_sale_watch_store.constant;

public class APIConstant {

    public static final String API_VERSION_ONE = "/v1";
    public static final String API_PUBLIC = "/public";
    public static final String API_MEMBER = "/member";
    public static final String API_ADMIN = "/admin";
    public static final String API_ROLE = "/role";
    public static final String API_USER = "/user";
    public static final String API_BRAND = "/brand";
    public static final String API_CATEGORY = "/category";
    public static final String API_PRODUCT = "/product";
    public static final String API_PRODUCT_DETAIL = "/product-detail";
    public static final String API_PRODUCT_IMAGE = "/product-image";
    public static final String API_ORDER = "/order";
}
