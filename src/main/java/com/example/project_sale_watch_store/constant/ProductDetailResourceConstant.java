package com.example.project_sale_watch_store.constant;

public class ProductDetailResourceConstant {

    public final static String PRD_CM_01 = "Create Success";
    public final static String PRD_CM_02 = "Update Success";
    public final static String PRD_CM_03 = "Delete Success";
    public final static String PRD_CM_04 = "Get Product Detail Success";
    public final static String PRD_CM_05 = "Get All Success";
    public final static String PRD_CM_06 = "Product Detail Not Found";
}
