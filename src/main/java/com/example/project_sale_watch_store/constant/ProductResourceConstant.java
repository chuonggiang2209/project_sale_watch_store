package com.example.project_sale_watch_store.constant;

public class ProductResourceConstant {

    public final static String PR_CM_01 = "Create Success";
    public final static String PR_CM_02 = "Update Success";
    public final static String PR_CM_03 = "Delete Success";
    public final static String PR_CM_04 = "Product not found";
    public final static String PR_CM_05 = "Product list not found";
    public final static String PR_CM_06 = "Product name is duplicated";
    public final static String PR_CM_07 = "Get products success";
}
