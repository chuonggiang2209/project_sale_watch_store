package com.example.project_sale_watch_store.constant;

public class SystemConstant {

    public static final String ACTIVE_STATUS = "ACTIVE";
    public static final String NO_ACTIVE_STATUS = "NO_ACTIVE";
    public static final Boolean IS_DELETED = true;

    public static final Boolean IS_NOT_DELETED = false;

    public static final String TOKEN_IS_NOT_VALID = "token is not valid";

    ;
}
