package com.example.project_sale_watch_store.constant;

public class CategoryResourceConstant {

    public final static String CA_CM_01 = "Create Success";
    public final static String CA_CM_02 = "Update Success";
    public final static String CA_CM_03 = "Delete Success";
    public final static String CA_CM_04 = "Category not found";
    public final static String CA_CM_05 = "Category list not found";
    public final static String CA_CM_06 = "Category name is duplicated";
    public final static String CA_CM_07 = "Get categories success";
}
