package com.example.project_sale_watch_store.constant;

public class ProductImageResourceConstant {

    public static String PI_CM_01 = "Create success";
    public static String PI_CM_02 = "Update success";
    public static String PI_CM_03 = "Delete success";
    public static String PI_CM_04 = "Get image success";
    public static String PI_CM_05 = "Get all image success";
    public static String PI_CM_06 = "Get image by id success";
    public static String PI_CM_07 = "Files are not allowed";
    public static String PI_CM_08 = "Images is null";
}
