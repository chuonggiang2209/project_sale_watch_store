package com.example.project_sale_watch_store.constant;

public class UserResourceConstant {

    public final static String US_CM_01 = "Create Success";
    public final static String US_CM_02 = "Update Success";
    public final static String US_CM_03 = "Delete Success";
    public final static String US_CM_04 = "Email is duplicated";
    public final static String US_CM_05 = "User not found";
    public final static String US_CM_06 = "Login success";
    public final static String US_CM_07 = "Login fail";
    public final static String US_CM_08 = "User list not found";
    public final static String US_CM_09 = "Get Users success";
    public final static String US_CM_10 = "User not active";

    public final static String TOK_CM_01 = "Token is not valid";

}
