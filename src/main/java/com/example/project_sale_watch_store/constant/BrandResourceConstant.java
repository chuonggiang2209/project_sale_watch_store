package com.example.project_sale_watch_store.constant;

public class BrandResourceConstant {

    public static final String BR_CM_01 = "Create Success";
    public static final String BR_CM_02 = "Update Success";
    public static final String BR_CM_03 = "Delete Success";
    public static final String BR_CM_04 = "Brand not found";
    public static final String BR_CM_05 = "Brand list not found";
    public static final String BR_CM_06 = "Brand name is duplicated";
    public static final String BR_CM_07 = "Get Brands success";
}
