package com.example.project_sale_watch_store.constant;

public class ResourceConstant {

    public final static String CM_01 = "CM_01";
    public final static String CM_02 = "CM_02";
    public final static String CM_03 = "CM_03";
    public final static String CM_04 = "CM_04";
    public final static String CM_05 = "CM_05";
    public final static String CM_06 = "CM_06";
    public final static String CM_07 = "CM_07";
    public final static String CM_08 = "CM_08";
    public final static String CM_09 = "CM_09";
    public final static String CM_10 = "CM_10";
}
