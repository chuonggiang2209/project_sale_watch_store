package com.example.project_sale_watch_store.constant;

public class OrderResourceConstant {

    public static final String OR_CM_01 = "Create success";
    public static final String OR_CM_02 = "Update success";
    public static final String OR_CM_03 = "Delete success";
    public static final String OR_CM_04 = "Get orders success";
    public static final String OR_CM_05 = "Order not found";
    public static final String OR_CM_06 = "Order list not found";
    public static final String ORDER_STATUS_PENDING = "PENDING";
    public static final String ORDER_STATUS_APPROVED = "APPROVED";
    public static final String ORDER_STATUS_CANCEL = "CANCEL";
}
