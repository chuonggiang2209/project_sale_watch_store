package com.example.project_sale_watch_store.productimage.repo;

import com.example.project_sale_watch_store.productimage.entity.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductImageRepo extends JpaRepository<ProductImage, UUID> {
}
