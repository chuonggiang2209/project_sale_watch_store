package com.example.project_sale_watch_store.productimage.service;

import com.example.project_sale_watch_store.productimage.dto.request.ProductImageAddRequest;
import com.example.project_sale_watch_store.utils.response.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface ProductImageService {

    ResponseEntity<Response> addProductImage(MultipartFile[] files, UUID id);
}
