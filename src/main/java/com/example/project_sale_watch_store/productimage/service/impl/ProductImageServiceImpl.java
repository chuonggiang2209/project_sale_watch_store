package com.example.project_sale_watch_store.productimage.service.impl;

import com.example.project_sale_watch_store.constant.ProductImageResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.product.service.ProductService;
import com.example.project_sale_watch_store.productimage.dto.response.ProductImageByteDTOResponse;
import com.example.project_sale_watch_store.productimage.entity.ProductImage;
import com.example.project_sale_watch_store.productimage.mapper.ProductImageDTOMapper;
import com.example.project_sale_watch_store.productimage.repo.ProductImageRepo;
import com.example.project_sale_watch_store.productimage.service.ProductImageService;
import com.example.project_sale_watch_store.utils.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class ProductImageServiceImpl implements ProductImageService {

    private final ProductService productService;

    private final ProductImageRepo productImageRepo;

    private final ProductImageDTOMapper productImageDTOMapper;

    @Override
    public ResponseEntity<Response> addProductImage(MultipartFile[] files, UUID id) {

        Product product = productService.getProductById(id);

        File dir = new File("src/main/resources/static/images");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        List<ProductImageByteDTOResponse> byteDTOResponses = new ArrayList<>();

        try {
            for (MultipartFile file : files
            ) {
                if (file.isEmpty())
                    throw new ApiRequestException(ResourceConstant.CM_08, ProductImageResourceConstant.PI_CM_08);

                String fileName = file.getOriginalFilename();
                System.out.println(file.getContentType());
                if (file.getContentType().endsWith("image/png") || file.getContentType().endsWith("image/jpg") || file.getContentType().endsWith("image/jpeg")) {

                    if (file.getContentType().endsWith("image/png")) {
                        fileName = UUID.randomUUID().toString() + ".png";
                    } else if (file.getContentType().endsWith("image/jpg")) {
                        fileName = UUID.randomUUID().toString() + ".jpg";
                    } else if (file.getContentType().endsWith("image/jpeg")) {
                        fileName = UUID.randomUUID().toString() + ".jpeg";
                    }

                    ProductImageByteDTOResponse byteDTOResponse = ProductImageByteDTOResponse.builder()
                            .name(fileName)
                            .image(file.getBytes())
                            .build();
                    byteDTOResponses.add(byteDTOResponse);
                } else {
                    throw new ApiRequestException(ResourceConstant.CM_07, ProductImageResourceConstant.PI_CM_07);
                }
            }

            for (ProductImageByteDTOResponse byteDTOResponse : byteDTOResponses
            ) {

                Path path = Paths.get("src/main/resources/static/images/" + byteDTOResponse.getName());
                Files.write(path, byteDTOResponse.getImage());
                ProductImage productImage = ProductImage.builder()
                        .product(product)
                        .imageUrl(byteDTOResponse.getName())
                        .isDeleted(SystemConstant.IS_NOT_DELETED)
                        .status(SystemConstant.ACTIVE_STATUS)
                        .build();
                productImageRepo.save(productImage);
            }

            Response response = Response.builder()
                    .code(ResourceConstant.CM_01)
                    .message(ProductImageResourceConstant.PI_CM_01)
                    .data(productImageDTOMapper.apply(product, byteDTOResponses))
                    .status(HttpStatus.CREATED.value())
                    .build();

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
