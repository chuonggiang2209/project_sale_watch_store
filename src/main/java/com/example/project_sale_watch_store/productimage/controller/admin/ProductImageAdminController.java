package com.example.project_sale_watch_store.productimage.controller.admin;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.productimage.dto.request.ProductImageAddRequest;
import com.example.project_sale_watch_store.productimage.service.ProductImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_PRODUCT_IMAGE)
public class ProductImageAdminController {

    private final ProductImageService productImageService;

    @PostMapping(consumes = {"multipart/form-data"}, produces = {"application/json"})
    public ResponseEntity<?> addProductImage(@RequestParam MultipartFile[] files, @RequestParam UUID id) {
        return productImageService.addProductImage(files, id);
    }
}
