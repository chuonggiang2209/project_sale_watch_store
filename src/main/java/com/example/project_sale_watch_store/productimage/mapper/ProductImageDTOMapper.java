package com.example.project_sale_watch_store.productimage.mapper;

import com.example.project_sale_watch_store.product.entity.Product;
import com.example.project_sale_watch_store.productimage.dto.ProductImageDTO;
import com.example.project_sale_watch_store.productimage.dto.response.ProductImageByteDTOResponse;
import com.example.project_sale_watch_store.productimage.entity.ProductImage;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ProductImageDTOMapper implements Function<ProductImage, ProductImageDTO> {

    @Override
    public ProductImageDTO apply(ProductImage productImage) {
        return ProductImageDTO.builder()
                .productId(productImage.getProduct().getId())
                .build();
    }

    public ProductImageDTO apply(Product product, List<ProductImageByteDTOResponse> imageUrl) {
        return ProductImageDTO.builder()
                .productId(product.getId())
                .imageUrl(imageUrl.stream().map(ProductImageByteDTOResponse::getName).collect(Collectors.toList()))
                .build();
    }
}
