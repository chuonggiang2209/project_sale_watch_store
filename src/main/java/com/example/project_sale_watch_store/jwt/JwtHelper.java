package com.example.project_sale_watch_store.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Date;

@Component
public class JwtHelper {

    @Value("${jwt.secret-key}")
    private String secretKey;

    public String generateToken(Object object) {

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
        return Jwts.builder()
                .claim("object", object)
                .expiration(new Date(8 * 60 * 60 * 1000 + new Date().getTime()))
                .signWith(key)
                .compact();
    }

    public Object ExtractToken(String token) {

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
        return Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token)
                .getPayload()
                .get("object");
    }
}
