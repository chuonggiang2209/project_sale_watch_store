package com.example.project_sale_watch_store.role.service.impl;

import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.RoleResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.role.dto.RoleDTO;
import com.example.project_sale_watch_store.role.dto.request.RoleAddRequest;
import com.example.project_sale_watch_store.role.entity.Role;
import com.example.project_sale_watch_store.role.mapper.RoleDTOMapper;
import com.example.project_sale_watch_store.role.repo.RoleRepo;
import com.example.project_sale_watch_store.role.service.RoleService;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class RoleServiceImpl implements RoleService {

    private final RoleRepo roleRepo;

    private final RoleDTOMapper roleDtoMapper;


    @Override
    public ResponseEntity<Response> addRole(RoleAddRequest roleAddRequest) {

        if (roleAddRequest.getName() == null || roleAddRequest.getName().isEmpty())
            throw new ApiRequestException(ResourceConstant.CM_05, RoleResourceConstant.RO_CM_05);

        if (roleRepo.existsByName(roleAddRequest.getName().toUpperCase()))
            throw new ApiRequestException(ResourceConstant.CM_04, RoleResourceConstant.RO_CM_04);

        Role role = Role.builder()
                .name(roleAddRequest.getName().toUpperCase())
                .status(SystemConstant.NO_ACTIVE_STATUS)
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .build();
        roleRepo.save(role);

        Response response = Response.builder()
                .status(HttpStatus.CREATED.value())
                .code(ResourceConstant.CM_01)
                .data(roleDtoMapper.apply(role))
                .message(RoleResourceConstant.RO_CM_01)
                .build();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateRole(RoleAddRequest roleAddRequest, UUID id) {

        if (roleAddRequest.getName() == null || roleAddRequest.getName().isEmpty())
            throw new ApiRequestException(ResourceConstant.CM_05, RoleResourceConstant.RO_CM_05);

        Role role = roleRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, RoleResourceConstant.RO_CM_06));
        role.setName(roleAddRequest.getName().toUpperCase());
        role.setStatus(roleAddRequest.getStatus());
        role.setIsDeleted(roleAddRequest.getIsDeleted());
        roleRepo.save(role);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_02)
                .data(roleDtoMapper.apply(role))
                .message(RoleResourceConstant.RO_CM_02)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteRole(UUID id) {

        Role role = roleRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, RoleResourceConstant.RO_CM_06));
        role.setIsDeleted(SystemConstant.IS_DELETED);
        role.setStatus(SystemConstant.NO_ACTIVE_STATUS);
        roleRepo.save(role);

        Response response = Response.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_03)
                .data(roleDtoMapper.apply(role))
                .message(RoleResourceConstant.RO_CM_03)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public Optional<Role> getRole(String name) {

        if (name == null || name.isEmpty())
            throw new ApiRequestException(ResourceConstant.CM_05, RoleResourceConstant.RO_CM_05);

        return Optional.ofNullable(roleRepo.findRoleByNameAndIsDeletedAndStatus(name.toUpperCase(), SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, RoleResourceConstant.RO_CM_06)));
    }

    @Override
    public ResponseEntity<Response> getRoles(Integer currentPage, Integer limitPage) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Role> all = roleRepo.findAllByIsDeletedAndStatus(SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_08, RoleResourceConstant.RO_CM_08));
        List<RoleDTO> roleDTOS = all.stream()
                .map(roleDtoMapper)
                .toList();

        Responses responses = Responses.builder()
                .status(HttpStatus.OK.value())
                .code(ResourceConstant.CM_09)
                .data(roleDTOS)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .message(RoleResourceConstant.RO_CM_09)
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
