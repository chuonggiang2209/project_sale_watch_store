package com.example.project_sale_watch_store.role.service;

import com.example.project_sale_watch_store.role.dto.request.RoleAddRequest;
import com.example.project_sale_watch_store.role.entity.Role;
import com.example.project_sale_watch_store.utils.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.UUID;

public interface RoleService {

    ResponseEntity<Response> addRole(RoleAddRequest roleAddRequest);

    ResponseEntity<Response> updateRole(RoleAddRequest roleAddRequest, UUID id);

    ResponseEntity<Response> deleteRole(UUID id);

    Optional<Role> getRole(String name);

    ResponseEntity<Response> getRoles(Integer currentPage, Integer limitPage);
}
