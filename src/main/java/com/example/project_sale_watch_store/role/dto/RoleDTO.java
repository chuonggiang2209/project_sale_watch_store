package com.example.project_sale_watch_store.role.dto;

import com.example.project_sale_watch_store.utils.base.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class RoleDTO extends BaseDTO {

    private String name;

    private String status;

    private Boolean isDeleted;
}
