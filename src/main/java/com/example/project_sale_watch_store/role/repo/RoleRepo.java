package com.example.project_sale_watch_store.role.repo;

import com.example.project_sale_watch_store.role.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleRepo extends JpaRepository<Role, UUID> {

    Boolean existsByName(String name);

    Optional<Page<Role>> findAllByIsDeletedAndStatus(Boolean isDeleted, String status, Pageable pageable);

    Optional<Role> findRoleByNameAndIsDeletedAndStatus(String name, Boolean isDeleted, String status);
}
