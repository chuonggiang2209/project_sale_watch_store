package com.example.project_sale_watch_store.role.controller.admin;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.role.dto.request.RoleAddRequest;
import com.example.project_sale_watch_store.role.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_ROLE)
public class RoleAdminController {

    private final RoleService roleService;

    @PostMapping
    public ResponseEntity<?> addRole(@RequestBody RoleAddRequest roleAddRequest) {
        return roleService.addRole(roleAddRequest);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateRole(@RequestBody RoleAddRequest roleAddRequest, @PathVariable UUID id) {
        return roleService.updateRole(roleAddRequest, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRole(@PathVariable UUID id) {
        return roleService.deleteRole(id);
    }

    @GetMapping
    public ResponseEntity<?> getRoles(@RequestParam Optional<Integer> currentPage, @RequestParam Optional<Integer> limitPage) {
        return roleService.getRoles(currentPage.orElse(1), limitPage.orElse(8));
    }
}
