package com.example.project_sale_watch_store.role.mapper;

import com.example.project_sale_watch_store.role.dto.RoleDTO;
import com.example.project_sale_watch_store.role.entity.Role;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class RoleDTOMapper implements Function<Role, RoleDTO> {

    @Override
    public RoleDTO apply(Role role) {
        return RoleDTO.builder()
                .id(role.getId())
                .status(role.getStatus())
                .isDeleted(role.getIsDeleted())
                .name(role.getName())
                .createdBy(role.getCreatedBy())
                .createdDate(role.getCreatedDate())
                .lastModifiedBy(role.getLastModifiedBy())
                .lastModifiedDate(role.getLastModifiedDate())
                .build();
    }
}
