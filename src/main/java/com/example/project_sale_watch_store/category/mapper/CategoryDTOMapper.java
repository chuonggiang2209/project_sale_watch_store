package com.example.project_sale_watch_store.category.mapper;

import com.example.project_sale_watch_store.category.dto.CategoryDTO;
import com.example.project_sale_watch_store.category.entity.Category;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class CategoryDTOMapper implements Function<Category, CategoryDTO> {

    @Override
    public CategoryDTO apply(Category category) {
        return CategoryDTO.builder()
                .id(category.getId())
                .name(category.getName())
                .isDeleted(category.getIsDeleted())
                .status(category.getStatus())
                .build();
    }
}
