package com.example.project_sale_watch_store.category.repo;

import com.example.project_sale_watch_store.category.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CategoryRepo extends JpaRepository<Category, UUID> {

    Boolean existsByName(String name);

    Optional<Page<Category>> findAllByIsDeletedAndStatus(Boolean isDeleted, String status, Pageable pageable);
}
