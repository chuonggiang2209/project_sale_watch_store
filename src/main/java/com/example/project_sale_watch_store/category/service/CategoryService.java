package com.example.project_sale_watch_store.category.service;

import com.example.project_sale_watch_store.category.dto.request.CategoryAddRequest;
import com.example.project_sale_watch_store.category.entity.Category;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface CategoryService {

    ResponseEntity<Response> addCategory(CategoryAddRequest categoryAddRequest);

    ResponseEntity<Response> updateCategory(CategoryAddRequest categoryAddRequest, UUID id);

    ResponseEntity<Response> deleteCategory(UUID id);

    ResponseEntity<Responses> getCategories(Integer currentPage, Integer limitPage);

    Category getCategoryById(UUID id);

    Boolean checkCategoryName(String name);
}
