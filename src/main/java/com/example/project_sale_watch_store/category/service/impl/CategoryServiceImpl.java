package com.example.project_sale_watch_store.category.service.impl;

import com.example.project_sale_watch_store.category.dto.CategoryDTO;
import com.example.project_sale_watch_store.category.dto.request.CategoryAddRequest;
import com.example.project_sale_watch_store.category.entity.Category;
import com.example.project_sale_watch_store.category.mapper.CategoryDTOMapper;
import com.example.project_sale_watch_store.category.repo.CategoryRepo;
import com.example.project_sale_watch_store.category.service.CategoryService;
import com.example.project_sale_watch_store.constant.CategoryResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepo categoryRepo;

    private final CategoryDTOMapper categoryDTOMapper;

    @Override
    public ResponseEntity<Response> addCategory(CategoryAddRequest categoryAddRequest) {

        if (checkCategoryName(categoryAddRequest.getName().toLowerCase()))
            throw new ApiRequestException(ResourceConstant.CM_06, CategoryResourceConstant.CA_CM_06);

        Category category = Category.builder()
                .name(categoryAddRequest.getName().toLowerCase())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.NO_ACTIVE_STATUS)
                .build();
        categoryRepo.save(category);

        Response response = Response.builder()
                .code(ResourceConstant.CM_01)
                .message(CategoryResourceConstant.CA_CM_01)
                .data(categoryDTOMapper.apply(category))
                .status(HttpStatus.CREATED.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> updateCategory(CategoryAddRequest categoryAddRequest, UUID id) {

        Category category = getCategoryById(id);
        category.setStatus(categoryAddRequest.getStatus());
        category.setIsDeleted(categoryAddRequest.getIsDeleted());
        categoryRepo.save(category);

        Response response = Response.builder()
                .code(ResourceConstant.CM_02)
                .message(CategoryResourceConstant.CA_CM_02)
                .data(categoryDTOMapper.apply(category))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteCategory(UUID id) {

        Category category = getCategoryById(id);
        category.setIsDeleted(SystemConstant.IS_DELETED);
        category.setStatus(SystemConstant.NO_ACTIVE_STATUS);
        categoryRepo.save(category);

        Response response = Response.builder()
                .code(ResourceConstant.CM_03)
                .message(CategoryResourceConstant.CA_CM_03)
                .data(categoryDTOMapper.apply(category))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Responses> getCategories(Integer currentPage, Integer limitPage) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Category> all = categoryRepo.findAllByIsDeletedAndStatus(SystemConstant.IS_NOT_DELETED, SystemConstant.ACTIVE_STATUS, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_05, CategoryResourceConstant.CA_CM_05));
        List<CategoryDTO> list = all.stream()
                .map(categoryDTOMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_07)
                .message(CategoryResourceConstant.CA_CM_07)
                .data(list)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .status(HttpStatus.OK.value())
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public Boolean checkCategoryName(String name) {
        return categoryRepo.existsByName(name);
    }

    @Override
    public Category getCategoryById(UUID id) {
        return categoryRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_04, CategoryResourceConstant.CA_CM_04));
    }
}
