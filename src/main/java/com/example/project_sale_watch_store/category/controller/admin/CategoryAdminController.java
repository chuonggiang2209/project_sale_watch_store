package com.example.project_sale_watch_store.category.controller.admin;

import com.example.project_sale_watch_store.category.dto.request.CategoryAddRequest;
import com.example.project_sale_watch_store.category.service.CategoryService;
import com.example.project_sale_watch_store.constant.APIConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_CATEGORY)
public class CategoryAdminController {

    private final CategoryService categoryService;

    @PostMapping
    public ResponseEntity<?> addCategory(@RequestBody CategoryAddRequest categoryAddRequest) {
        return categoryService.addCategory(categoryAddRequest);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@RequestBody CategoryAddRequest categoryAddRequest, @PathVariable UUID id) {
        return categoryService.updateCategory(categoryAddRequest, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable UUID id) {
        return categoryService.deleteCategory(id);
    }

    @GetMapping
    public ResponseEntity<?> getCategories(@RequestParam Optional<Integer> currentPage, @RequestParam Optional<Integer> limitPage) {
        return categoryService.getCategories(currentPage.orElse(1), limitPage.orElse(8));
    }
}
