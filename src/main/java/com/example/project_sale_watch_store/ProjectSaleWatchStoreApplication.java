package com.example.project_sale_watch_store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSaleWatchStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSaleWatchStoreApplication.class, args);
    }

}
