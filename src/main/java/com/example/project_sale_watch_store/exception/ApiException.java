package com.example.project_sale_watch_store.exception;

public record ApiException(

        String code,
        String message,
        Integer status,
        Long responseTime

) {
}
