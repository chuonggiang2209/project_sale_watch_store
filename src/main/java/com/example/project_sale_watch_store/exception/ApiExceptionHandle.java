package com.example.project_sale_watch_store.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandle {

    @ExceptionHandler(value = ApiRequestException.class)
    protected ResponseEntity<Object> handleRuntimeException(ApiRequestException e) {
        //Tạo một object APIException mới
        ApiException apiException = new ApiException(
                e.getCode(),
                e.getMessage(),
                HttpStatus.BAD_REQUEST.value(),
                System.currentTimeMillis()
        );
        // Trả về response với thông tin exception mới
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }
}
