package com.example.project_sale_watch_store.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ApiRequestException extends RuntimeException {

    private String code;
    private String message;

    public ApiRequestException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
