package com.example.project_sale_watch_store.order.entity;


import com.example.project_sale_watch_store.orderdetail.entity.OrderDetail;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.utils.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
@Table(name = "_order")
public class Order extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    @Column(name = "order_status", nullable = false)
    private String orderStatus;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "total_price", nullable = false)
    private Double totalPrice;

    @Column(name = "email", nullable = false)
    private String email;

    @OneToMany(mappedBy = "order",fetch = FetchType.LAZY,orphanRemoval = true)
    private List<OrderDetail> orderDetails;
}
