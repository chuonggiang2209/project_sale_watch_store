package com.example.project_sale_watch_store.order.controller.member;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_MEMBER + APIConstant.API_VERSION_ONE + APIConstant.API_ORDER)
public class OrderMemberController {

    private final OrderService orderService;

    @GetMapping
    public ResponseEntity<?> getOrdersByUserId(@RequestParam Optional<Integer> currentPage,
                                               @RequestParam Optional<Integer> limitPage) {
        return orderService.getOrdersByUserId(currentPage.orElse(1), limitPage.orElse(8));
    }
}
