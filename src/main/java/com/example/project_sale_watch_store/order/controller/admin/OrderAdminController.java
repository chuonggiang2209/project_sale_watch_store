package com.example.project_sale_watch_store.order.controller.admin;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.constant.OrderResourceConstant;
import com.example.project_sale_watch_store.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_ADMIN + APIConstant.API_VERSION_ONE + APIConstant.API_ORDER)
public class OrderAdminController {

    private final OrderService orderService;

    @GetMapping
    public ResponseEntity<?> getOrdersByOrderStatus(@RequestParam Optional<Integer> currentPage,
                                                    @RequestParam Optional<Integer> limitPage,
                                                    @RequestParam Optional<String> orderStatus) {

        return orderService.getOrdersByOrderStatus(currentPage.orElse(1), limitPage.orElse(8), orderStatus.orElse(OrderResourceConstant.ORDER_STATUS_PENDING));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> approveOrder(@PathVariable UUID id) {

        return orderService.approveOrder(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable UUID id) {

        return orderService.deleteOrder(id);
    }
}
