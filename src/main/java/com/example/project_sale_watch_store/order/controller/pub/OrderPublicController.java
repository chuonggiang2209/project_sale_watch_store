package com.example.project_sale_watch_store.order.controller.pub;

import com.example.project_sale_watch_store.constant.APIConstant;
import com.example.project_sale_watch_store.order.dto.request.OrderAddRequest;
import com.example.project_sale_watch_store.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(APIConstant.API_PUBLIC + APIConstant.API_VERSION_ONE + APIConstant.API_ORDER)
public class OrderPublicController {

    private final OrderService orderService;

    @PostMapping
    public ResponseEntity<?> addOrder(@RequestBody OrderAddRequest orderAddRequest) {
        return orderService.addOrder(orderAddRequest);
    }
}
