package com.example.project_sale_watch_store.order.repo;

import com.example.project_sale_watch_store.order.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface OrderRepo extends JpaRepository<Order, UUID> {

    Optional<Page<Order>> findAllByOrderStatus(String orderStatus, Pageable pageable);

    Optional<Page<Order>> findAllByUserId(UUID id, Pageable pageable);
}
