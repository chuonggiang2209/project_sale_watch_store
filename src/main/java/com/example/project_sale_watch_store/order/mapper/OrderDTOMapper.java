package com.example.project_sale_watch_store.order.mapper;

import com.example.project_sale_watch_store.order.dto.OrderDTO;
import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.orderdetail.mapper.OrderDetailDTOMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class OrderDTOMapper implements Function<Order, OrderDTO> {

    private final OrderDetailDTOMapper orderDetailDTOMapper;

    @Override
    public OrderDTO apply(Order order) {
        return OrderDTO.builder()
                .id(order.getId())
                .email(order.getEmail())
                .orderDate(order.getOrderDate())
                .orderStatus(order.getOrderStatus())
                .address(order.getAddress())
                .phoneNumber(order.getPhoneNumber())
                .totalPrice(order.getTotalPrice())
                .isDeleted(order.getIsDeleted())
                .status(order.getStatus())
                .detail(order.getOrderDetails().stream().map(orderDetailDTOMapper).collect(Collectors.toList()))
                .build();
    }

    public OrderDTO applyDetail(Order order, List<OrderDetailDTO> orderDetailDTOS) {
        return OrderDTO.builder()
                .id(order.getId())
                .email(order.getEmail())
                .orderDate(order.getOrderDate())
                .orderStatus(order.getOrderStatus())
                .address(order.getAddress())
                .phoneNumber(order.getPhoneNumber())
                .totalPrice(order.getTotalPrice())
                .isDeleted(order.getIsDeleted())
                .status(order.getStatus())
                .detail(orderDetailDTOS)
                .build();
    }
}
