package com.example.project_sale_watch_store.order.dto.request;

import com.example.project_sale_watch_store.orderdetail.dto.request.OrderDetailAddRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class OrderAddRequest {

    private String email;

    private String address;

    private String phoneNumber;

    private Double totalPrice;

    private List<OrderDetailAddRequest> detail;
}
