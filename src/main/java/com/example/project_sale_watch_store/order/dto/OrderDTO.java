package com.example.project_sale_watch_store.order.dto;

import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.user.entity.User;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class OrderDTO {

    private UUID id;

    private String email;

    private Date orderDate;

    private String orderStatus;

    private String address;

    private String phoneNumber;

    private Double totalPrice;

    private String status;

    private Boolean isDeleted;

    private List<OrderDetailDTO> detail;
}
