package com.example.project_sale_watch_store.order.service.impl;

import com.example.project_sale_watch_store.constant.OrderResourceConstant;
import com.example.project_sale_watch_store.constant.ResourceConstant;
import com.example.project_sale_watch_store.constant.SystemConstant;
import com.example.project_sale_watch_store.exception.ApiRequestException;
import com.example.project_sale_watch_store.order.dto.OrderDTO;
import com.example.project_sale_watch_store.order.dto.request.OrderAddRequest;
import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.order.mapper.OrderDTOMapper;
import com.example.project_sale_watch_store.order.repo.OrderRepo;
import com.example.project_sale_watch_store.order.service.OrderService;
import com.example.project_sale_watch_store.orderdetail.dto.OrderDetailDTO;
import com.example.project_sale_watch_store.orderdetail.service.OrderDetailService;
import com.example.project_sale_watch_store.user.entity.User;
import com.example.project_sale_watch_store.user.service.UserService;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;

    private final UserService userService;

    private final OrderDTOMapper orderDTOMapper;

    private final OrderDetailService orderDetailService;

    @Override
    public ResponseEntity<Response> addOrder(OrderAddRequest orderAddRequest) {

        User user = userService.getUser();

        Order order = Order.builder()
                .orderDate(new Date())
                .address(orderAddRequest.getAddress())
                .phoneNumber(orderAddRequest.getPhoneNumber())
                .totalPrice(orderAddRequest.getTotalPrice())
                .orderStatus(OrderResourceConstant.ORDER_STATUS_PENDING)
                .user(user)
                .email(orderAddRequest.getEmail())
                .isDeleted(SystemConstant.IS_NOT_DELETED)
                .status(SystemConstant.NO_ACTIVE_STATUS)
                .build();
        orderRepo.saveAndFlush(order);

        List<OrderDetailDTO> list = orderAddRequest.getDetail().stream()
                .map(
                        o -> orderDetailService.addProductDetail(o, order)
                )
                .toList();


        Response response = Response.builder()
                .code(ResourceConstant.CM_01)
                .status(HttpStatus.CREATED.value())
                .message(OrderResourceConstant.OR_CM_01)
                .data(orderDTOMapper.applyDetail(order, list))
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Response> approveOrder(UUID id) {

        Order order = getOrderById(id);

        order.setIsDeleted(SystemConstant.IS_NOT_DELETED);
        order.setStatus(SystemConstant.ACTIVE_STATUS);
        order.setOrderStatus(OrderResourceConstant.ORDER_STATUS_APPROVED);

        Response response = Response.builder()
                .code(ResourceConstant.CM_02)
                .status(HttpStatus.OK.value())
                .message(OrderResourceConstant.OR_CM_02)
                .data(orderDTOMapper.apply(order))
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteOrder(UUID id) {

        Order order = getOrderById(id);

        order.setIsDeleted(SystemConstant.IS_DELETED);
        order.setStatus(SystemConstant.NO_ACTIVE_STATUS);
        order.setOrderStatus(OrderResourceConstant.ORDER_STATUS_CANCEL);

        Response response = Response.builder()
                .code(ResourceConstant.CM_03)
                .status(HttpStatus.OK.value())
                .message(OrderResourceConstant.OR_CM_03)
                .data(orderDTOMapper.apply(order))
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public Order getOrderById(UUID id) {

        return orderRepo.findById(id)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_05, OrderResourceConstant.OR_CM_05));
    }

    @Override
    public ResponseEntity<Responses> getOrdersByOrderStatus(Integer currentPage, Integer limitPage, String orderStatus) {

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Order> all = orderRepo.findAllByOrderStatus(orderStatus, pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, OrderResourceConstant.OR_CM_06));
        List<OrderDTO> list = all.stream()
                .map(orderDTOMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_04)
                .status(HttpStatus.OK.value())
                .message(OrderResourceConstant.OR_CM_04)
                .data(list)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Responses> getOrdersByUserId(Integer currentPage, Integer limitPage) {

        User user = userService.getUser();

        Pageable pageable = PageRequest.of(currentPage - 1, limitPage);
        Page<Order> all = orderRepo.findAllByUserId(user.getId(), pageable)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.CM_06, OrderResourceConstant.OR_CM_06));
        List<OrderDTO> list = all.stream()
                .map(orderDTOMapper)
                .toList();

        Responses responses = Responses.builder()
                .code(ResourceConstant.CM_04)
                .status(HttpStatus.OK.value())
                .message(OrderResourceConstant.OR_CM_04)
                .data(list)
                .meta(new Responses.PageableResponses(all.getTotalPages(), pageable))
                .build();

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }


}
