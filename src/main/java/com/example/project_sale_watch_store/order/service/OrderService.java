package com.example.project_sale_watch_store.order.service;

import com.example.project_sale_watch_store.order.dto.request.OrderAddRequest;
import com.example.project_sale_watch_store.order.entity.Order;
import com.example.project_sale_watch_store.utils.response.Response;
import com.example.project_sale_watch_store.utils.response.Responses;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface OrderService {

    ResponseEntity<Response> addOrder(OrderAddRequest orderAddRequest);

    ResponseEntity<Response> approveOrder( UUID id);

    ResponseEntity<Response> deleteOrder(UUID id);

    Order getOrderById(UUID id);

    ResponseEntity<Responses> getOrdersByOrderStatus(Integer currentPage, Integer limitPage , String orderStatus);

    ResponseEntity<Responses> getOrdersByUserId(Integer currentPage, Integer limitPage);
}
